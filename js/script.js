$(document).ready(function(){

	ajustaMenu();

	$("html").niceScroll({
		scrollspeed: 80
	});

	$("#slide_content").cycle({
        speed: 2000,
        timeout: 100,
        next: '#slide_proximo',
        prev: '#slide_anterior',
        fx: 'fade', 
	});

	$(document).scroll(function(){
		ajustaMenu();
	});

	function ajustaMenu() { 

		posicao = $(document).scrollTop();
		if (posicao != 0) {
			$('#header').addClass('header_fixo');
			$('#header div div img').animate({
				height: '50',
				width: '130',
			}, {
				queue: false
			});
			$('#header').animate({
				height: '60'
			}, {
				queue: false
			});			
		} else {		
			$('#header').removeClass('header_fixo');
			$('#header div div img').animate({
				height: '90',
				width: '200',
			}, {
				queue: false
			});
			$('#header').animate({
				height: '100'
			}, {
				queue: false
			});
		}
	}
});